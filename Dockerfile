FROM ruby:2.7.3

RUN apt-get update -qq && apt-get install -y build-essential

# Install honcho
RUN apt-get install -y python-pip
RUN pip install honcho

# Install clamav
RUN apt-get install -y clamav clamav-daemon

COPY docker/etc/systemd/system/clamav-freshclam.service /etc/systemd/system/clamav-freshclam.service
COPY docker/etc/systemd/system/clamav-daemon.service /etc/systemd/system/clamav-daemon.service

# Install nginx
RUN apt-get install -y nginx

COPY ./docker/etc/nginx/sites-available/http /etc/nginx/sites-available/http
COPY ./docker/etc/nginx/sites-available/https /etc/nginx/sites-available/https

RUN rm /etc/nginx/sites-enabled/default
RUN ln -s /etc/nginx/sites-available/http /etc/nginx/sites-enabled/http
RUN ln -s /etc/nginx/sites-available/https /etc/nginx/sites-enabled/https

# Install letsencrypt
RUN apt-get install -y letsencrypt

# Cleanup after apt runs
RUN rm -rf /var/lib/apt/lists/*

# Install clamav-rest app
ENV APP_HOME /app
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

ADD Gemfile* $APP_HOME/
RUN bundle config set without 'development test'
RUN bundle

ADD . $APP_HOME

EXPOSE 80 443

ENV PYTHONUNBUFFERED 1

ENTRYPOINT ["./entrypoint.sh"]

CMD ["honcho", "start"]
