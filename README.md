## How do I run the server?

    $> rackup -p 3000 -o 0.0.0.0

## How to build the docker image?

    $> docker build -t clamav-rest .

## How to run the docker image?

    $> docker run -p 8080:4567 clamav-rest

## How to bash into the image?

    $> docker run -it clamav-rest bash

## How do I bash into a running container?

    $> docker exec -it <CONTAINER_NAME> bash

## Where is the git repo?

    https://bitbucket.org/beegoodit/clamav-rest

## Where is the dockerhub repo?

    https://hub.docker.com/repository/docker/beegoodit/clamav-rest

## How do I enable http basic auth?

    $> docker run -e HTTP_BASIC_AUTH_USERNAME=username -e HTTP_BASIC_AUTH_PASSWORD=password -p 80:80 -p 443:443 clamav-rest

## How do I enabled letsencrypt

    $> docker run -e LETSENCRYPT_DOMAIN=eu-scanner.tatoru.io -e LETSENCRYPT_EMAIL=info@tatoru.io -p 80:80 -p 443:443 clamav-rest

## How to deploy a new node on digital ocean?

1. Create a new node

2. Open port 8080:

    ssh $> ufw allow 80/tcp
    ssh $> ufw allow 443/tcp

3. Login docker:

    ssh $> docker login (beegoodit/********)

4. Pull tage image

    ssh $> docker pull beegoodit/clamav-rest:latest

4. Run the container
 
    ssh $> docker run \
           --restart always -d \
           -e LETSENCRYPT_DOMAIN=eu-scanner.tatoru.io \
           -e LETSENCRYPT_EMAIL=info@tatoru.io \
           -e HTTP_BASIC_AUTH_USERNAME=eu-api \
           -e HTTP_BASIC_AUTH_PASSWORD=2ed69190bd1243b350a63c4baa4eb3cb \
           -p 80:80 \
           -p 443:443 \
           beegoodit/clamav-rest:latest

## How do I update a node?

    ssh $> docker stop <CONTAINER_NAME>
    ssh $> docker pull beegoodit/clamav-rest
    ssh $> docker run --restart always -d \
          -e LETSENCRYPT_DOMAIN=eu-scanner.tatoru.io \
          -e LETSENCRYPT_EMAIL=info@tatoru.io \
          -e HTTP_BASIC_AUTH_USERNAME=eu-api \
          -e HTTP_BASIC_AUTH_PASSWORD=2ed69190bd1243b350a63c4baa4eb3cb \
          -p 80:80 \
          -p 443:443 \
          beegoodit/clamav-rest:latest
