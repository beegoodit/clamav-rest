module ClamavRestApi
  class App < Sinatra::Base
    extend Sinatra::BasicAuth
    include Sinatra::BasicAuth::Helpers

    authorize do |username, password|
      if ENV.has_key?("HTTP_BASIC_AUTH_USERNAME") && ENV.has_key?("HTTP_BASIC_AUTH_PASSWORD")
        username == ENV.fetch("HTTP_BASIC_AUTH_USERNAME") && password == ENV.fetch("HTTP_BASIC_AUTH_PASSWORD")
      else
        true
      end
    end

    protect do
      post '/scans' do
        content_type :json

        if params.has_key?('url')
          content = URI.open(params['url']).read
          filename = params['url'].split("/").last
        else
          content = params['file']
          filename = File.basename(params['filename'].to_s)
        end

        file = Tempfile.new(enconding: "ascii-8bit")
        file.write(content)
        file.rewind

        scan_result = ::ClamavRestApi::Scanner.new(file.path).perform

        response = {
          infected: scan_result.infected?,
          matches: scan_result.matches,
          filename: filename,
          content_length: file.size,
          sha1: Digest::SHA1.hexdigest(file.read)
        }

        file.close
        file.unlink
        
        response.to_json
      end
    end

    protect do
      get '/info' do
        content_type :json

        response = {}
        
        stdout, stderr, status = Open3.capture3("uptime")
        response[:uptime] = stdout.strip
        stdout, stderr, status = Open3.capture3("clamscan --version")
        response[:clamscan_version] = stdout.strip

        response[:processes] = []
        pids = Dir.glob("/proc/[0-9]*")
        pids.each do |pid|
          command = begin
            File.read(pid + "/comm")
          rescue Errno::ENOENT
          end
          response[:processes] << { pid: pid.split("/").last, command: command.strip }
        end
        
        response.to_json
      end
    end
  end
end
