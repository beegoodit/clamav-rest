require 'rubygems'
require 'bundler'

Bundler.require(:default)
Bundler.require(Sinatra::Base.environment)

require 'rack'
require 'sinatra'
require 'sinatra/base'
require 'sinatra/basic_auth'
require 'json'
require 'open-uri'
require 'open3'

Dir[File.join(__dir__, 'initializers', '*.rb')].each {|file| require file }

require_relative '../lib/clamav_rest_api'
require_relative '../lib/clamav_rest_api/scanner'
require_relative '../app/clamav_rest_api/app'
