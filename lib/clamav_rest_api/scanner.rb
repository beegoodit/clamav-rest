module ClamavRestApi
  class Scanner
    class Result
      attr_accessor :matches, :duration, :errors, :infected

      def infected?
        infected
      end

      def clean?
        !infected?
      end
    end

    def initialize(path)
      @path = path
      @result = Result.new
    end

    def perform
      perform_scan!
      build_result
    end

    private

    def perform_scan!
      @stdout, @stderr, @status = Open3.capture3("clamdscan #{File.expand_path(@path)} --fdpass --stream")
    end

    def build_result
      if @stderr.length > 0
        @result.errors = @stderr
        return @result
      end

      splitted_stdout = @stdout.split("\n")
      
      @result.matches = splitted_stdout[0].split(": ")[1..-1].map do |m|
        next if m == "OK"
        m.slice!(" FOUND")
        m
      end.compact
      @result.duration = splitted_stdout[4].split[1]
      @result.infected = splitted_stdout[3].split[-1] != "0"

      @result
    end
  end
end
