#!/bin/bash

if [[ -z "${LETSENCRYPT_EMAIL}" ]]; then
  echo "# Create temporary certificate #################################################"
  mkdir -p /etc/ssl/certs

  C="DE"
  ST="State"
  L="Location"
  O="Organization"
  OU="Organizational Unit"
  CN="localhost"

  openssl req -x509 -sha256 -nodes -newkey rsa:2048 -days 365 -keyout /etc/ssl/certs/server.key -out /etc/ssl/certs/server.crt -subj "/C=$C/ST=$ST/L=$L/OU=$OU/CN=$CN"
else
  echo "# Create letsenrypt certificate ################################################"
  certbot --text --email $LETSENCRYPT_EMAIL -d $LETSENCRYPT_DOMAIN --agree-tos --standalone certonly -n
  ln -s /etc/letsencrypt/live/$LETSENCRYPT_DOMAIN/fullchain.pem /etc/ssl/certs/server.crt
  ln -s /etc/letsencrypt/live/$LETSENCRYPT_DOMAIN/privkey.pem /etc/ssl/certs/server.key
fi


echo "# Setup folders for clamav #####################################################"
mkdir -p /var/run/clamav/
chown -R clamav.clamav /var/run/clamav/

echo "# Download clamav signatures ###################################################"
freshclam
