require "sinatra_helper"

RSpec.describe ClamavRestApi::App do
  def app
    described_class
  end

  before(:all) do
    ENV["HTTP_BASIC_AUTH_USERNAME"] = "jane"
    ENV["HTTP_BASIC_AUTH_PASSWORD"] = "doe"
  end

  describe "GET /info" do
    describe "unauthorized access" do
      before(:each) do
        get "/info"
      end

      it { expect(last_response.status).to eq(401) }
      it { expect(last_response.body).to eq("Authorization Required") }
    end

    describe "authorized access" do
      let(:headers) { { "Content-Type" => "application/x-www-form-urlencoded" } }

      let(:json) { JSON.parse(last_response.body) }

      before(:each) do
        basic_authorize "jane", "doe"
        get "/info", nil, headers: headers
      end

      it { expect(last_response.status).to eq(200) }
      it { expect(json).to be_a(Hash) }
      it { expect(json.keys).to match_array(%w(uptime clamscan_version processes)) }
    end
  end

  describe "POST /scans" do
    describe "unauthorized access" do
      before(:each) do
        post "/scans"
      end

      it { expect(last_response.status).to eq(401) }
      it { expect(last_response.body).to eq("Authorization Required") }
    end

    describe "with a url" do
      let(:headers) { { "Content-Type" => "application/x-www-form-urlencoded" } }

      let(:json) { JSON.parse(last_response.body) }

      before(:each) do
        basic_authorize "jane", "doe"
        post "/scans", params, headers: headers
      end

      describe "response" do
        let(:params) { { url: "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" } }

        it { expect(last_response.status).to eq(200) }
        it { expect(json).to be_a(Hash) }
        it { expect(json.keys).to match_array(%w(content_length filename infected matches sha1)) }
      end

      describe "infected" do
        let(:params) { { url: "https://secure.eicar.org/eicar.com.txt" } }

        it { expect(json['infected']).to be_truthy }
        it { expect(json['matches']).to eq(["Win.Test.EICAR_HDB-1"]) }
      end

      describe "clean" do
        let(:params) { { url: "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" } }

        it { expect(json['infected']).to be_falsey }
      end
    end

    describe "with a file" do
      let(:headers) { { "Content-Type" => "application/x-www-form-urlencoded" } }

      let(:json) { JSON.parse(last_response.body) }
      
      let(:filename) { ClamavRestApi.root.join(*%w(spec files clean.pdf)) }
      let(:body) { { file: File.open(filename).read, filename: filename } }

      before(:each) do
        basic_authorize "jane", "doe"
        post "/scans", body, headers: headers
      end

      describe "response" do
        it { expect(last_response.status).to eq(200) }
        it { expect(json).to be_a(Hash) }
        it { expect(json.keys).to match_array(%w(content_length filename infected matches sha1)) }
        it { expect(json['filename']).to eq(File.basename(filename)) }
      end

      describe "infected" do
        let(:filename) { ClamavRestApi.root.join(*%w(spec files infected.zip)) }

        it { expect(json['infected']).to be_truthy }
      end

      describe "clean" do
        let(:filename) { ClamavRestApi.root.join(*%w(spec files clean.pdf)) }

        it { expect(json['infected']).to be_falsey }
      end
    end
  end
end
