ENV['RACK_ENV'] = 'test'
require "./config/environment"

RSpec.configure do |config|
  config.include Rack::Test::Methods
end
